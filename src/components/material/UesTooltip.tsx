import React, {useState} from 'react';
import {makeStyles, Theme, Tooltip, TooltipProps, useMediaQuery} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
    maxWidth: 260,
    fontSize: 11,
    fontWeight: 400,
    padding: theme.spacing(0.75, 1),
  },
}));

const UesTooltip: React.FC<TooltipProps> = (props) => {
  const classes = useStyles();
  
  return <Tooltip arrow classes={classes} {...props} />;
};

export default UesTooltip;
