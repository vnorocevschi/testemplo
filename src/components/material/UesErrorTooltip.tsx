import React from 'react';
import { makeStyles } from '@material-ui/core';
import { ErrorIcon } from 'svgs';
import UesTooltip from './UesTooltip';

interface UesErrorTooltipProps {
  className?: string;
  title?: string;
}

const useStyles = makeStyles(() => {
  return {
    svg: {
      pointerEvents: 'auto',
      height: 19,
      fill: '#ABAEB5',
    },
  };
});

const UesErrorTooltip: React.FC<UesErrorTooltipProps> = ({ title }) => {
  const classes = useStyles();

  return (
    <>
    <UesTooltip placement="top" title={title}>
      <span>
        <ErrorIcon className={classes.svg}/>
      </span>
    </UesTooltip>
      </>
  );
};

export default UesErrorTooltip;
