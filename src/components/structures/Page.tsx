import React, { FC, ReactNode } from 'react';
import Head from 'next/head';
import { Layout } from 'containers';
import {makeStyles} from "@material-ui/core";
import ErrorAlert from "../../containers/common/ErrorAlert";

interface PageProps {
  title?: string;
  children: ReactNode;
}

const useStyles = makeStyles(
  () => {
    return {
      root: {
        paddingTop: 65,
        paddingBottom: 60,
        minHeight: '100vh',
        position: 'relative',
        backgroundColor: '#fff',
      }
    }
  }
);

const Page: FC<PageProps> = ({ title = 'Hype Shop', children }) => {
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ErrorAlert/>
      <Layout>{children}</Layout>
    </div>
  );
};

export default Page;