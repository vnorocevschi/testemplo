import React from 'react';
import {
  BaseTextFieldProps,
  FilledInputProps,
  InputProps,
  makeStyles,
  TextField,
  Theme, Typography,
} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import {UesErrorTooltip} from 'components/material';

interface UesTextInputProps extends BaseTextFieldProps {
  spaces?: boolean;
  errorSpace?: boolean;
  formatter?: (value: any) => string;
  inputProps?: InputProps['inputProps'];
  InputProps?: Partial<FilledInputProps>;
  onChange: (value: any) => void;
  onFocus?: React.FocusEventHandler;
  onBlur?: React.FocusEventHandler;
  errorMessage?: string;
}

const StyledTextInput = withStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
  },
}))(TextField);

const useStyle = makeStyles((theme: Theme) => {
  return {
    wrapper: {
      display: 'inline',
      position: 'relative',
    },
    errorIcon: {
      position: 'absolute',
      top: 21,
      right: 18,
      height: 19,
    },
    errorIconSpace: {
      top: 8,
    },
    withError: {
      '& input': {
        paddingRight: 36,
      },
    },
    errorMessage: {
      position: 'absolute',
      top: 62,
      fontSize: 12,
      width: 'calc(100% - 32px)',
      display: 'block',
      backgroundColor: '#ba4949',
      left: 16,
      textAlign: 'center',
      color: 'white',
      borderRadius: '0 0 8px 8px',
      padding: theme.spacing(0.25, 1.25, 0),
    },
  };
});

const UesTextInput: React.FC<UesTextInputProps> = ({
                                                     spaces = false,
                                                     InputProps,
                                                     InputLabelProps,
                                                     onChange,
                                                     errorMessage,
                                                     errorSpace,
                                                     ...props
                                                   }) => {
  const classes = useStyle();
  if(errorSpace) {console.log(errorSpace)}
  return (
    <div className={classes.wrapper}>
      <StyledTextInput
        {...props}
        fullWidth
        margin={spaces ? 'dense' : 'none'}
        variant="filled"
        InputProps={{...InputProps, disableUnderline: true}}
        autoComplete="nope"
        InputLabelProps={{...InputLabelProps, shrink: true}}
        onChange={(e) => onChange(e.target.value)}
        className={errorMessage ? classes.withError : ''}
      />
      {!!errorMessage && (
        <>
          <div className={clsx(classes.errorIcon, !!errorSpace && classes.errorIconSpace)}>
            <UesErrorTooltip title={errorMessage}/>
          </div>
        </>
      )}
    </div>
  );
};

export default UesTextInput;
