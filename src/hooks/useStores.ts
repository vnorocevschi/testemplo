import { useContext } from 'react';
import { storeContext } from 'providers/StoreProvider';
import StoreContainer from 'stores/StoreContainer';

const useStores = () => useContext<StoreContainer>(storeContext);

export default useStores;
