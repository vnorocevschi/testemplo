import { makeStyles, Theme } from '@material-ui/core';

export const createRootStyles = () =>
  makeStyles(
    (theme: Theme) => ({
      noPadding: {
        padding: theme.spacing(0),
      },
      noMargin: {
        margin: theme.spacing(0),
      },
      displayBlock: {
        display: 'block',
      },
      fullWidth: {
        width: '100%',
      },
      textUppercase: {
        textTransform: 'uppercase',
      },
      textOverflow: {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
      titleText: {
        fontSize: 24,
        fontWeight: 300,
        color: '#333',
        [theme.breakpoints.up('sm')]: {
          fontSize: 35,
        },
      },
    }),
    {
      name: 'RootStyles',
    },
  );