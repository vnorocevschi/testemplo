export const TEST_EMPLOYEES_ENDPOINT = `/test_employees`;
export const TEST_EMPLOYEES_QUERY_ENDPOINT = (query: string) => `${TEST_EMPLOYEES_ENDPOINT}?${query}`;
export const SINGLE_EMPLOYEE_ENDPOINT = (employeeId: number) =>
  `${TEST_EMPLOYEES_ENDPOINT}/${employeeId}`;