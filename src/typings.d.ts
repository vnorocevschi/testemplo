import { PropsWithChildren } from 'react';

declare global {
  interface GenericObject {
    [props: string]: any;
  }

  interface Window {}

  type Primitive = string | number | boolean;

  type Nullable<T> = T | null | undefined;

  type HookProps<T> = PropsWithChildren<T>;
}
