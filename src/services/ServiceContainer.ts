import { DiContainer } from 'utils/di';
import { HttpClient } from './http';
import { HttpClientInterface } from './http/types';
import {ItemsServiceInterface} from "./items/types";
import ItemsService from "./items/ItemsService";
import {ServiceContainerInterface} from "./types";

export const createApiBaseUrl = (): string => process.env.REACT_APP_API_BASE_URL;

class ServiceContainer extends DiContainer implements ServiceContainerInterface{

  get itemsService(): ItemsServiceInterface {
    return this.getInstance('itemsService', () => new ItemsService(this.httpClient));
  }

  private get httpClient(): HttpClientInterface {
    return this.getInstance('httpClient', () => new HttpClient({ baseUrl: createApiBaseUrl() }));
  }

}

export default ServiceContainer;
