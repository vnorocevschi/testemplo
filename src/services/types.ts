import {ItemsServiceInterface} from 'services/items/types';

export interface ServiceContainerInterface {
  readonly itemsService: ItemsServiceInterface;
}
