import axios, { AxiosInstance } from 'axios';
import { HttpClientConfig, HttpClientInterface, HttpRequest, HttpResponse } from './types';

class HttpClient implements HttpClientInterface {
  protected axios: AxiosInstance;

  constructor(config: HttpClientConfig = {}) {
    const { baseUrl = '', adapter } = config;

    this.axios = axios.create({
      baseURL: baseUrl,
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (adapter) {
      this.axios.defaults.adapter = adapter;
    }
  }

  createUrl = (endpoint: string): string => {
    let url = this.axios.defaults.baseURL || '';
    if (url.endsWith('/')) {
      url = url.substr(0, url.length - 1);
    }

    return `${url}${endpoint}`;
  };

  get<TResponse>(url: string, queryParams: any = {}, config: HttpRequest): Promise<TResponse> {
    return this.axios.get(url, { ...config, params: queryParams });
  }

  post<TResponse>(url: string, bodyParams?: any, config: HttpRequest = {}): Promise<TResponse> {
    return this.axios.post(url, bodyParams, config);
  }

  patch<TResponse>(url: string, bodyParams?: any, config: HttpRequest = {}): Promise<TResponse> {
    return this.axios.patch(url, bodyParams, config);
  }

  put<TResponse>(url: string, bodyParams?: any, config?: HttpRequest): Promise<TResponse> {
    return this.axios.put(url, bodyParams, config);
  }

  delete<TResponse>(url: string, config?: HttpRequest): HttpResponse<TResponse> {
    return this.axios.delete(url, config);
  }
}

export default HttpClient;
