interface meta {
  total_count: number;
  filter_count: number;
}

export interface EmployeeItem{
  id: number;
  name: string;
  position: string;
  age: number;
}

export interface GetEmployeeResponse {
  meta: meta;
  data: Array<EmployeeItem>
}

export interface FormEmployeeRequest {
  name: string;
  position: string;
  age: number;
}

export interface OrderCreateResponse {
  status: boolean;
  redirect_url: string;
}

export interface ItemsServiceInterface {
  getEmployees(query: string): Promise<GetEmployeeResponse>;
  deleteEmployeeById(id: number): Promise<void>;
  createEmployee(form: FormEmployeeRequest): Promise<void>;
}