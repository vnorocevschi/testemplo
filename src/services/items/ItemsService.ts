import {
  FormEmployeeRequest,
  GetEmployeeResponse,
  ItemsServiceInterface,
} from "services/items/types";
import {HttpClientInterface} from "services/http/types";
import {
  SINGLE_EMPLOYEE_ENDPOINT,
  TEST_EMPLOYEES_ENDPOINT, TEST_EMPLOYEES_QUERY_ENDPOINT
} from "config/items/endpoints";

class ItemsService implements ItemsServiceInterface {
  constructor(private readonly client: HttpClientInterface) {
  }
  
  async deleteEmployeeById(employeeId: number): Promise<void> {
    const { data } = await this.client.delete<void>(SINGLE_EMPLOYEE_ENDPOINT(employeeId));
    
    return data;
  }
  
  async getEmployees(query: string): Promise<GetEmployeeResponse> {
    const {data} = await this.client.get<GetEmployeeResponse>(TEST_EMPLOYEES_QUERY_ENDPOINT(query));

    return data;
  }
  
  async createEmployee(form: FormEmployeeRequest): Promise<void> {
    const {data} = await this.client.post<void>(TEST_EMPLOYEES_ENDPOINT, form);
    
    return data;
  }
}

export default ItemsService;