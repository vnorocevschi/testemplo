import React, { FC } from 'react';
import { SvgIconProps } from '@material-ui/core';

import ErrorCircleSvg from 'svgs/error-circle.svg';
import CloseSvg from 'svgs/close.svg';
import GreyCloseSvg from 'svgs/close-grey.svg';
import ErrorLgSvg from 'svgs/error-circle-lg.svg';

export const ErrorIcon: FC<SvgIconProps> = React.forwardRef((props, ref) => (
  <ErrorCircleSvg ref={ref} {...props} />
));

export const CloseIcon: FC<SvgIconProps> = React.forwardRef((props, ref) => (
  <CloseSvg ref={ref} {...props} />
));

export const GreyCloseIcon: React.FC<SvgIconProps> = React.forwardRef((props, ref) => (
	<GreyCloseSvg ref={ref} {...props} />
));
export const ErrorLgIcon: React.FC<SvgIconProps> = React.forwardRef((props, ref) => (
	<ErrorLgSvg ref={ref} {...props} />
));