export interface FormValues {
  [key: string]: any;
}

export interface FormErrors {
  [key: string]: any;
}

export type TErrors<Values> = { [key in keyof Values]: string };

export interface FormFieldFilter {
  (value: any): any;
}

export type FormFieldFilters = FormFieldFilter | Array<FormFieldFilter>;

export interface FormFilters {
  [field: string]: FormFieldFilters;
}

export type FormValidatorResult = [boolean, string | any[]];

export interface FormFieldValidator<Values = FormValues> {
  (value: any, values?: Values): FormValidatorResult;
}

export type FormFieldValidators<Values> =
  | FormFieldValidator<Values>
  | Array<FormFieldValidator<Values>>;

export interface FormValidators<Values> {
  [field: string]: FormFieldValidators<Values>;
}
