import { FormFieldValidator, FormValidatorResult, FormValidators } from 'utils/form/types';
import { validateArray } from 'utils/form';

const AGE_REGEX = /^\d{1,2}$/;
const ENGLISH_REGEX = /^([A-Za-z]+[,.]?[ ]?|[A-Za-z]+['-]?)+$/;
const ENGLISH_WITH_SYMBOLS_REGEX = /^([A-Za-z\d#№/&-]+[,.]?[ ]?|[A-Za-z]+['-]?)+$/;

export const isRequired = (errorMessage = 'Пожалуйста заполните данное поле'): FormFieldValidator => (
  value: any
): FormValidatorResult => [![null, undefined, ''].includes(value), errorMessage];

export const isEnglish = (errorMessage = 'Введите пожалуйста на английском языке'): FormFieldValidator => (
  value: any
): FormValidatorResult => [ENGLISH_REGEX.test(value), errorMessage];

export const isEnglishWithSymbols = (errorMessage = 'Введите пожалуйста на английском языке'): FormFieldValidator => (
  value: any
): FormValidatorResult => [ENGLISH_WITH_SYMBOLS_REGEX.test(value), errorMessage];

export const isAge = (errorMessage = 'Введите возраст цифрами'): FormFieldValidator => (
  value: any
): FormValidatorResult => [AGE_REGEX.test(value), errorMessage];

export const arrayValidator = <Values>(
  valuesValidators: FormValidators<Values>
): FormFieldValidator => (arrayOfValues: Values[]): FormValidatorResult => {
  return validateArray<Values>(valuesValidators, arrayOfValues);
};
