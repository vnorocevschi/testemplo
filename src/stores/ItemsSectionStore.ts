import {action, observable} from 'mobx';
import {EmployeeItem, ItemsServiceInterface} from "services/items/types";
import AlertStore from "./AlertStore";

interface InitialState {
	data: Data;
}

interface TableSettings {
	page: number;
	rowPerPage: number;
	sort: keyof typeof sort;
	totalItems: number;
	isLoading: boolean;
}

interface Data {
	items: Array<EmployeeItem>;
	tableSettings: TableSettings;
}

enum sort {
	ascPos = 'position',
	descPos = '-position',
	ascAge = 'age',
	descAge = '-age',
	ascName = 'name',
	descName = '-name',
	ascId = 'id',
	descId = '-id',
}

class ItemsSectionStore {
	initialData: Data = observable({
		tableSettings: {page: 0, rowPerPage: 4, totalItems: 1, sort: 'descId', isLoading: true},
		items: []
	});
	data: Data = observable({items: this.initialData.items, tableSettings: this.initialData.tableSettings});
	prefixLink: String = 'product/';
	
	constructor(
		private readonly itemsService: ItemsServiceInterface,
		private readonly alert: AlertStore,
	) {
	}
	
	@action getItems = async (page: number): Promise<void> => {
			this.data.tableSettings.isLoading = true;
		try {
			const {data, meta} = await this.itemsService.getEmployees(`meta=*&sort=${sort[this.data.tableSettings.sort]}&limit=${this.data.tableSettings.rowPerPage}&offset=${page * this.data.tableSettings.rowPerPage}`);
			this.data.tableSettings.totalItems = meta.filter_count;
			this.data.items = data;
		} catch (e) {
			this.alert.showAlert('We can\'t load employee, please try again', e);
			console.log(e);
		} finally {
			this.data.tableSettings.isLoading = false;
		}
	};
	
	@action deleteItem = async (id: number): Promise<void> => {
		try {
			await this.itemsService.deleteEmployeeById(id);
			await this.handleChangePage(undefined, 0);
		} catch (e) {
			this.alert.showAlert(e);
		}
	};
	
	@action handleChangePage = async (event, newPage) => {
		this.data.tableSettings.page = newPage;
		await this.getItems(newPage);
	};
	
	@action setSort = async (row: 'age' | 'name' | 'position' | 'id') => {
		switch (row) {
			case "age": this.data.tableSettings.sort === 'ascAge' ? this.data.tableSettings.sort = "descAge" : this.data.tableSettings.sort = 'ascAge';
			break;
			case "name": this.data.tableSettings.sort === 'ascName' ? this.data.tableSettings.sort = "descName" : this.data.tableSettings.sort = 'ascName';
			break;
			case "position": this.data.tableSettings.sort === 'ascPos' ? this.data.tableSettings.sort = "descPos" : this.data.tableSettings.sort = 'ascPos';
			break;
			case "id": this.data.tableSettings.sort === 'ascId' ? this.data.tableSettings.sort = "descId" : this.data.tableSettings.sort = 'ascId';
			break;
		}
		await this.handleChangePage(undefined, 0);
	};
	
	@action handleChangeRowsPerPage = async (event) => {
		this.data.tableSettings.rowPerPage = parseInt(event.target.value, 10);
		this.data.tableSettings.page = 0;
		await this.getItems(0);
	};
	
	@action initState = (initialState: InitialState): void => {
		this.data.items = initialState.data.items;
		this.data.tableSettings = initialState.data.tableSettings;
	};
	
}

export default ItemsSectionStore;
