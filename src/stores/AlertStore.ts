import {action, observable} from 'mobx';

// interface AlertMessage {
//   title: string;
//   text?: string;
//   onClose?: Function;
// }

class AlertStore {
  // errorMessage: string = observable('');
  // message?: AlertMessage = observable(null);
  data = observable({
    errorMessage: '',
  });

  showAlert = action((errorMessage: string, e?: any) => {
    this.data.errorMessage = errorMessage;
    e && console.error(e);
  });

  closeAlert = action(() => {
    // this.errorMessage?.onClose && this.errorMessage.onClose();
    this.data.errorMessage = '';
  });
/*
  showSuccessAlert = action((message: AlertMessage, e?: any) => {
    this.message = message;
    e && console.error(e);
  });

  closeSuccessAlert = action(() => {
    this.message?.onClose && this.message.onClose();
    this.message = undefined;
  });
 */
}

export default AlertStore;
