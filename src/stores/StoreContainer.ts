import { DiContainer } from 'utils/di';
import {ServiceContainerInterface} from "services/types";
import ItemsSectionStore from "stores/ItemsSectionStore";
import ItemPageStore from "stores/ItemPageStore";
import AlertStore from "stores/AlertStore";

class CommercialStoreContainer extends DiContainer {
  constructor(private readonly services: ServiceContainerInterface) {
    super();
  }
  
  get alertStore(): AlertStore {
    return this.getInstance('alertStore', () => new AlertStore());
  }

  initState = (initialState): void => {
    if (initialState) {
      for (const [storeName, storeInitialState] of Object.entries(initialState)) {
        this[storeName].initState(storeInitialState);
      }
    }
  };

  get itemsSectionStore(): ItemsSectionStore {
    const { itemsService } = this.services;

    return this.getInstance('itemsSectionStore', () => new ItemsSectionStore(itemsService, this.alertStore));
  }

  get itemPageStore(): ItemPageStore {
    const { itemsService } = this.services;

    return this.getInstance('itemPageStore', () => new ItemPageStore(itemsService, this.alertStore));
  }
}

export default CommercialStoreContainer;
