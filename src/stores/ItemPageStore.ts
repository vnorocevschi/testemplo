import {action, observable} from 'mobx';
import {ItemsServiceInterface} from "services/items/types";
import {EmployeeForm} from "interfaces/item";
import {validateValue, validateValues} from "utils/form";
import {isAge, isEnglish, isEnglishWithSymbols, isRequired} from "utils/form/validators";
import {FormValidators} from "utils/form/types";
import AlertStore from "stores/AlertStore";

type Errors = Partial<EmployeeForm>;

const initialForm: EmployeeForm = {
	position: '',
	name: '',
	age: ''
};

class ItemPageStore {
	data = observable({
		form: initialForm,
		requestLoading: false,
		errors: {},
	});
	
	constructor(
		private readonly itemsService: ItemsServiceInterface,
		private readonly alert: AlertStore,
	) {
	}
	
	get form(): EmployeeForm {
		return this.data.form;
	}
	
	get requestLoad(): boolean {
		return this.data.requestLoading;
	}
	
	get errors(): Errors {
		return this.data.errors;
	}
	
	saveForm = action(async (): Promise<void> => {
		if (!this.isValid()) {
			return;
		}
		
		this.data.requestLoading = true;
		try {
			await this.itemsService.createEmployee({age: +this.form.age, name: this.form.name, position: this.form.position});
			this.initState();
		} catch (e) {
			this.alert.showAlert('We can\'t create employee, please try again', e);
		} finally {
			this.data.requestLoading = false;
		}
		// this.setTab(2); // next Tab
	});
	
	isValid = action((): boolean => {
		const [isValid, errors] = validateValues<Errors>(this.validators, this.form);
		this.data.errors = errors;
		
		return isValid;
	});
	
	handleUpdate = action((prop: keyof EmployeeForm, value: string) => {
		this.data.form[prop] = value;
		
		const [, error] = validateValue<EmployeeForm>(this.validators[prop], this.form[prop]);
		this.data.errors[prop] = String(error);
	});
	
	initState = action((): void => {
		this.data.form = initialForm;
		this.data.errors = {};
	});
	
	private get validators(): FormValidators<EmployeeForm> {
		return {
			position: [isRequired(), isEnglishWithSymbols()],
			name: [isRequired(), isEnglish()],
			age: [isRequired(), isAge()]
		};
	}
}

export default ItemPageStore;
