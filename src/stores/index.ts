import StoreContainer from 'stores/StoreContainer';
import ServiceContainer from "services/ServiceContainer";

export default new StoreContainer(new ServiceContainer());