import React, {FC} from 'react';
import {observer} from 'mobx-react-lite';
import {Grid, makeStyles, Theme, createStyles, Typography, MenuItem, Button, CircularProgress} from '@material-ui/core';
import { UesTextInput} from 'components/form-controls';
import {EmployeeForm} from "interfaces/item";

type Errors = Partial<EmployeeForm>;

interface OrderFormProps {
	form: EmployeeForm;
	handleUpdate: (prop: keyof EmployeeForm, value: string) => void;
	errors: Errors;
	disabledSubmit?: boolean;
	
	onSubmit: () => void;
}

const useStyles = makeStyles(
	(theme: Theme) => createStyles({
		buyButton: {
			display: 'block',
			outline: 'none',
			fontSize: 16,
			transition: 'all 0.4s',
			fontWeight: 600,
			textTransform: 'uppercase',
			backgroundColor: '#EF2828',
			maxWidth: 200,
			width: '100%',
			padding: theme.spacing(1.5, 1.75),
			color: '#fff',
			borderRadius: 10,
			margin: theme.spacing(1.25, 'auto', 0),
			'&:hover': {
				backgroundColor: 'rgb(239, 40, 40, .7)',
			},
			'&:disabled': {
				backgroundColor: '#f78686',
				color: 'rgba(255, 255, 255 , .7)',
			},
		},
		subPaymentInfo: {
			fontWeight: 600,
			color: '#fff',
			backgroundColor: '#d2d5db',
			padding: theme.spacing(0.75, 2),
			lineHeight: 1,
			borderRadius: 5,
			margin: theme.spacing(0, 'auto'),
			maxWidth: 'max-content',
		},
		circularLoading: {
			color: 'white',
			marginLeft: 10,
		},
		displayFlex: {
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
		}
	})
);

const OrderForm: FC<OrderFormProps> = observer((props) => {
	const classes = useStyles();
	const {form, handleUpdate, errors, onSubmit, disabledSubmit} = props;
	
	return (
		<Grid container spacing={2}>
			<Grid item xs={12} sm={6}>
				<UesTextInput
					label="Name"
					placeholder={'Name'}
					value={form.name}
					onChange={(value) => handleUpdate('name', value)}
					spaces
					required
					errorMessage={errors.name || ''}
					error={!!errors.name}
				/>
			</Grid>
			<Grid item xs={12} sm={6}>
				<UesTextInput
					label="Age"
					placeholder={'Age'}
					value={form.age}
					onChange={(value) => handleUpdate('age', value)}
					spaces
					required
					errorMessage={errors.age || ''}
					error={!!errors.age}
				/>
			</Grid>
			<Grid item xs={12} sm={6}>
				<UesTextInput
					label="Position"
					placeholder={'Position'}
					value={form.position}
					onChange={(value) => handleUpdate('position', value)}
					spaces
					required
					errorMessage={errors.position || ''}
					error={!!errors.position}
				/>
			</Grid>
			<Grid item xs={12}>
				<Button disabled={!!disabledSubmit} className={classes.buyButton} onClick={onSubmit}>
					<div className={classes.displayFlex}>Save{!!disabledSubmit &&
          <CircularProgress size={24} className={classes.circularLoading}/>}</div>
				</Button>
			</Grid>
		</Grid>
	);
});

export default OrderForm;
