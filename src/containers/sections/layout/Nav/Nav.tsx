import React, {FC} from 'react';
import AppBar from '@material-ui/core/AppBar';
import {Container, Grid, Theme} from '@material-ui/core';
import {makeStyles} from '@material-ui/core';
import Link from 'next/link';
import clsx from "clsx";

interface NavProps {
  window?: () => Window;
}

const useStyles = makeStyles((theme: Theme) => ({
  container: {},
  smooth: {
    transition: 'all 0.4s',
  },
  header: {
    minHeight: 65,
    border: 'none',
    backgroundColor: 'black',
  },
  logo: {
    lineHeight: '65px',
    color: '#fff',
    fontSize: 21,
    textTransform: 'uppercase',
  },
  linkElement: {
    lineHeight: '65px',
    color: '#fff',
    fontSize: 19,
  },
  logoColor: {
    color: '#000',
  },
  menuItem: {
    padding: 10,
    display: 'flex',
  },
}));

const Nav: FC<NavProps> = () => {
  const classes = useStyles();
  
  return (
    <AppBar
      className={clsx(classes.header, classes.smooth)}
      color='inherit'
      elevation={3}
    >
      <Container maxWidth={false} fixed>
        <Grid alignItems={'center'} justify="space-between" className={classes.container} container>
          <Grid item md={6}>
            <Link href={'/'}>
              <a title={'Home'} className={classes.logo}>
                Test Employees
              </a>
            </Link>
          </Grid>
          <Grid item md={6}>
            <Link href={'create'}>
              <a title={'Create Employee'} className={classes.linkElement}>
                Create
              </a>
            </Link>
          </Grid>
        </Grid>
      </Container>
    </AppBar>
  );
};

export default Nav;
