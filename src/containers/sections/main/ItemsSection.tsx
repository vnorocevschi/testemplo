import React, {useState} from 'react';
import {
	Button, CircularProgress,
	Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
	makeStyles,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TablePagination,
	TableRow,
	Theme,
} from '@material-ui/core';
import {observer} from "mobx-react-lite";
import {useStores} from "hooks";

const useStyles = makeStyles(
	(theme: Theme) => {
		return {
			itemsSection: {},
			trendSection: {
				backgroundColor: '#fff',
				padding: theme.spacing(2.5, 0, 4),
				marginBottom: 8,
				[theme.breakpoints.up('sm')]: {
					marginBottom: 12,
				},
				[theme.breakpoints.up('lg')]: {
					padding: theme.spacing(5, 0),
				},
			},
			listSection: {
				backgroundColor: '#f2f2f2',
				paddingBottom: theme.spacing(4),
				[theme.breakpoints.up('lg')]: {
					paddingBottom: theme.spacing(5),
				},
			},
			titleText: {
				fontWeight: 400,
				color: '#333',
				fontSize: 28,
				textAlign: 'center',
				[theme.breakpoints.up('sm')]: {
					fontSize: 40,
				},
				[theme.breakpoints.up('lg')]: {
					textAlign: 'left',
					fontSize: 48,
				}
			},
		}
	},
	{name: 'ItemsSection'},
);

const ItemsSection: React.FC = observer(() => {
	const classes = useStyles();
	
	const {itemsSectionStore} = useStores();
	const {setSort, deleteItem, data, handleChangePage, handleChangeRowsPerPage} = itemsSectionStore;
	const [open, setOpen] = React.useState(false);
	const [employeeId, setEmployeeId] = useState(0);
	
	const handleClickOpen = (id: number) => {
		setEmployeeId(id);
		setOpen(true);
	};
	
	const handleClose = () => {
		setOpen(false);
	};
	
	const handleCloseDelete = async () => {
		await deleteItem(employeeId);
		setOpen(false);
	};
	return (
		<div className={classes.itemsSection}>
			{data.tableSettings.isLoading && <CircularProgress size={100}/>}
	{!data.tableSettings.isLoading && !!data.items.length && (
				<>
					<TableContainer component={Paper}>
						<Table aria-label="simple table">
							<TableHead>
								<TableRow>
									<TableCell onClick={() => setSort("id")}>ID</TableCell>
									<TableCell onClick={() => setSort("name")}>Name</TableCell>
									<TableCell onClick={() => setSort("position")}>Position</TableCell>
									<TableCell onClick={() => setSort("age")} align="right">Age</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{data.items.map((item, index) => (
									<TableRow key={item.id}>
										<TableCell component="th" scope="row">
											<Button variant="outlined" onClick={() => handleClickOpen(item.id)}>
												{item.id}
											</Button>
											<Dialog
												open={open}
												onClose={handleClose}
												aria-labelledby="alert-dialog-title"
												aria-describedby="alert-dialog-description"
											>
												<DialogTitle id="alert-dialog-title">
													{"Do you want to delete a employee?"}
												</DialogTitle>
												<DialogContent>
													<DialogContentText id="alert-dialog-description">
														By click on Agree you confirm to delete a employee with id {employeeId}
													</DialogContentText>
												</DialogContent>
												<DialogActions>
													<Button onClick={handleClose}>Disagree</Button>
													<Button onClick={handleCloseDelete} autoFocus>
														Agree
													</Button>
												</DialogActions>
											</Dialog>
										</TableCell>
										<TableCell component="th" scope="row">
											{item.name}
										</TableCell>
										<TableCell>{item.position}</TableCell>
										<TableCell align="right">{item.age}</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
					<TablePagination
						rowsPerPageOptions={[4,10]}
						count={data.tableSettings.totalItems}
						rowsPerPage={data.tableSettings.rowPerPage}
						page={data.tableSettings.page}
						onChangePage={handleChangePage}
						onChangeRowsPerPage={handleChangeRowsPerPage}
						component="div"
					/>
				</>
			)}
		</div>
	);
});

export default ItemsSection;