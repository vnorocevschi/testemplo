import React, {FC} from 'react';
import {useStores} from "hooks";
import {OrderForm} from "containers/forms";
import {observer} from "mobx-react-lite";

const ItemProductSection: FC = observer(() => {
	const {itemPageStore} = useStores();
	const { handleUpdate, errors, form, requestLoad, saveForm} = itemPageStore;
	
	const sendData = async () => {
		await saveForm();
	};
	
	return (
					<OrderForm disabledSubmit={requestLoad} form={form} handleUpdate={handleUpdate} errors={errors}
					           onSubmit={sendData}/>
	);
});

export default ItemProductSection;