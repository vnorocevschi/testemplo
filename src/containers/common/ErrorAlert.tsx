import React from 'react';
import { IconButton, makeStyles, Snackbar } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import { ErrorLgIcon, GreyCloseIcon } from 'svgs';
import { withStyles } from '@material-ui/core/styles';
import {useStores} from "hooks";

const StyledSnackbar = withStyles(() => ({
  root: {
    top: 110,
    maxWidth: 754,
  },
}))(Snackbar);

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    padding: theme.spacing(1.5, 5, 1.5, 2.5),
    backgroundColor: 'white',
    fontSize: 16,
    fontWeight: 600,
  },
  close: {
    position: 'absolute',
    right: 15,
    top: 21,
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  errorIcon: {
    pointerEvents: 'auto',
    height: 27,
    width: '100%',
    maxWidth: 27,
    position: 'absolute',
    left: 14,
  },
  content: {
    display: 'flex',
    color: '#BA4949',
    paddingLeft: 32,
    maxWidth: 380,
  },
  flexClass: {
    flex: 1,
  },
}));

const ErrorAlert: React.FC = observer(() => {
  const classes = useStyles();
  const { alertStore } = useStores();
  const { data, closeAlert } = alertStore;
  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    closeAlert();
  };
  
  return (
    <StyledSnackbar
      ContentProps={{
        classes: {
          root: classes.root,
          message: classes.flexClass,
        },
      }}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={!!data.errorMessage.length}
      autoHideDuration={5000}
      onClose={handleClose}
      message={
        <div className={classes.content}>
          <ErrorLgIcon className={classes.errorIcon} />
          {data.errorMessage}
        </div>
      }
      action={
        <>
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose} className={classes.close}>
            <GreyCloseIcon fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
});

export default ErrorAlert;
