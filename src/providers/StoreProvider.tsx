import React, { createContext, FC, ReactNode } from 'react';
import StoreContainer from 'stores/StoreContainer';

interface Props {
  children: ReactNode;
  storeContainer: StoreContainer;
}

export const storeContext = createContext<StoreContainer>({} as StoreContainer);

const Provider = storeContext.Provider;

const StoreProvider: FC<Props> = ({ children, storeContainer }) => {
  return <Provider value={storeContainer}>{children}</Provider>;
};

export default StoreProvider;
