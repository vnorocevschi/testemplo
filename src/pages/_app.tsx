import React, { FC, useEffect } from 'react';
import { enableStaticRendering } from 'mobx-react-lite';
import { configure } from 'mobx';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { AppProps } from 'next/app';
import {createCrmTheme} from 'config/theme';
import storeContainer from 'stores';
import { StoreProvider } from 'providers';
import 'styles/globals.css';
import Head from "next/head";

configure({ enforceActions: 'observed' });
enableStaticRendering(typeof window === 'undefined');

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  storeContainer.initState(pageProps.initialState);
  const theme = createCrmTheme({});
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <>
    <Head>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>

      <StoreProvider storeContainer={storeContainer}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </StoreProvider>
    </>
  );
};

export default MyApp;