import React, {FC} from 'react';
import {Page} from 'components/structures';
import {makeStyles} from "@material-ui/core";
import ItemProductSection from "../../containers/sections/product/ItemProductSection";

const useStyles = makeStyles(
  () => {
    return {
      root: {
        paddingTop: 65,
      }
    }
  }
);

const Product: FC = () => {
  const classes = useStyles();
  
  return (
    <>
      <Page title={'Create employee | Test_Employees'}>
        <main className={classes.root}>
          <ItemProductSection/>
        </main>
      </Page>
    </>
  );
};

export default Product;