import React, {FC} from 'react';
import {Page} from 'components/structures';
import {observer} from 'mobx-react-lite';
import stores from 'stores';
import {
  ItemsSection,
} from 'containers/sections/main';

const Home: FC = observer(() => {
  
  return (
    <>
      <Page title={'Home Page | Test Employees'}>
        <main>
          <ItemsSection/>
        </main>
      </Page>
    </>
  );
});

export const getServerSideProps = async (): Promise<unknown> => {
  const {itemsSectionStore} = stores;
  const {getItems} = itemsSectionStore;
  await getItems(0);
  
  return {
    props: {
      initialState: {
        itemsSectionStore: {
          data: itemsSectionStore.data,
        },
      },
    },
  };
};


export default Home;