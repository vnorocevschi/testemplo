/* eslint-disable */
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const withImage = require('next-images');
const isProd = process.env.NODE_ENV === 'production';
const assetUrl = process.env.ASSET_URL || '';

module.exports = withImage({
  env: {
    REACT_APP_API_BASE_URL: 'https://admin.energy5.com/items',
    REACT_APP_MOCK_SERVICES: process.env.REACT_APP_MOCK_SERVICES,
  },
  exclude: path.resolve(__dirname, 'src/svgs'),
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });

    return config;
  },
  async redirects() {
    return [
      {
        source: '/product',
        destination: '/',
        permanent: true,
      }
    ];
  },
  assetPrefix: isProd ? assetUrl : '',
});
